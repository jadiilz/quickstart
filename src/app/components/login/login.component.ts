import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';

/**
 * Services
 */
import { AuthenticationService } from './../../modules/shared/services/laravel/authentication.service';
import { ResetPasswordComponent } from '../../modules/shared/components/reset-password/reset-password.component';
import { ForgotPasswordComponent } from '../../modules/shared/components/forgot-password/forgot-password.component';
import { CrudService } from '../../modules/shared/services/laravel/crud.service';
import { RegisterComponent } from '../register/register.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public disabled: boolean;
  obj: any;
  public loginForm: FormGroup;

  constructor(
    private _auth: AuthenticationService,
    private _router: Router,
    public _snackbar: MatSnackBar,
    private dialog: MatDialog,
    private _crud: CrudService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.disabled = false;

    const url = this._router.url;
    const urlArray = url.split('/');
    this._route.params
    .subscribe(params => {
      if (params.id && urlArray[1] + '/' + urlArray[2] === 'password/refresh') {
        this._crud.newRead({
          route: 'password?token=' + urlArray[3] + '&email=' + urlArray[4]
        })
        .then(res => {
          this.obj = {
            'email': res['obj']['data'],
            'token': urlArray[3]
          };
          //   if(this.obj.is_blocked) {
          //     this._snackbar.open('ERRO: Link de recuperação inválido.', '', {
          //       duration: 5000,
          //       panelClass: ['error-snackbar']
          //     })
          //     this._router.navigate(['/']);
          //   } else {
          this.onRecoverPassword();
          //   }
          // }
        }, rej => {
          this._snackbar.open('ERRO: Link de recuperação inválido.', '', {
            duration: 5000,
            panelClass: ['error-snackbar']
          });
          this._router.navigate(['/']);
        });
      }
      if (params.id && urlArray[1] === 'register') {
        this._crud.newRead({
          route: 'invitation/' + urlArray[2]
        }).then(resp => {
          this.obj = resp['obj'];
          this.onRegister();
        }, rej => {
          this._snackbar.open('Convite Inválido.', '', {
            duration: 4000,
            panelClass: ['error-snackbar']
          });
          // this._router.navigate(['/']);
        });
      }
    });

    this.loginForm = new FormGroup({
      user: new FormControl(null),
      password: new FormControl(null)
    });
  }

  onLoginSubmit = () => {
    // this.disabled = true;
    let params;
    params = {
      login: this.loginForm.get('user'),
      password: this.loginForm.get('password'),
      loginMode: 'emailAndPassword',
      navigateTo: '/main'
    };

    this._auth
    .login(params)
    .then(res => {
      const string = JSON.stringify(res);
      const json = JSON.parse(string);
      if (json.cod === 'l-01') {
        this._snackbar.open(json.message, '', {
          duration: 2000,
          panelClass: ['success-snackbar']
        });
        this._router.navigate(['/main']);
      }

      if (json.cod === 'le-01') {
        this._snackbar.open(json.message, '', {
          duration: 2000,
          panelClass: ['error-snackbar']
        });
      }
    }).catch( rej => {
      setTimeout(()  => {
        this.disabled = false;
      }, 3000);
    });
  }

  onRegister = () => {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '900px',
      data: this.obj,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {
      const array =  [];
      const string = '';
    });
  }

  onForgotPassword = () => {
    const dialogRef = this.dialog.open(ForgotPasswordComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      const array =  [];
      const string = '';
    });
  }

  onRecoverPassword = () => {
    const dialogRef = this.dialog.open(ResetPasswordComponent, {
      width: '900px',
      data: this.obj,
      disableClose: true
    });
  }
}
