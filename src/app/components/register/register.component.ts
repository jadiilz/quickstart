
import { Component, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators, FormArray, FormBuilder } from '@angular/forms';
import { MatSnackBar, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Services
 */
import { AuthenticationService } from './../../modules/shared/services/laravel/authentication.service';
import { CrudService } from './../../modules/shared/services/laravel/crud.service';
import { ValidateCnpj } from '../../modules/shared/validators/cnpj.validator';
import { ValidateCpf } from '../../modules/shared/validators/cpf.validator';
import { ValidateRequired } from '../../modules/shared/validators/required.validator';
import { ValidateValueComparison } from '../../modules/shared/validators/value-comparison.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public mask: any;
  public registerForm: FormGroup;
  public user: any;

  constructor(
    private _fb: FormBuilder,
    private _crud: CrudService,
    private _route: ActivatedRoute,
    private _router: Router,
    public _snackbar: MatSnackBar,
    public dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }

  ngOnInit() {
    this.user = this.data;

    this.mask = {
      cpf: [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/],
      date: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
      zip: [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/],
      phone: ['(', /\d/, /\d/, ')', ' ' , /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
      cell_phone: ['(', /\d/, /\d/, ')', ' ' , /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
      cnpj: [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
    };

    this.registerForm = new FormGroup({
      'name': new FormControl(null),
      'email': new FormControl(null, [Validators.email, ValidateRequired]),
      'password': new FormControl(null, ValidateRequired),
      'repeatPassword': new FormControl(null, ValidateRequired),
    });

    this.registerForm.get('password').setValidators([ValidateValueComparison(this.registerForm.get('repeatPassword'),
    'Campos nova senha e confirme nova senha precisam ter o mesmo valor'), ValidateRequired]);

    this.registerForm.controls.name.setValidators(ValidateRequired);
    this.registerForm.controls['name'].updateValueAndValidity();
    this.registerForm.get('email').setValue(this.user.email);
  }

  close = () => {
    this.dialogRef.close();
  }

  onRegisterSubmit = () => {
    delete this.registerForm.value['repeatPassword'];
    const params = {
      route: 'register?token=' + this.user.token,
      objectToCreate: this.registerForm.value
    };

    this._crud.create(params)
    .then(res => {
      const retorno: any = res;
      this.dialogRef.close();
      this._snackbar.open('Usuário cadastro com sucesso, realize login no sistema para começar a usar.', '', {
        duration: 4000,
        panelClass: ['success-snackbar']
      });

    }, rej => {
      this._snackbar.open(rej['message'], '', {
        duration: 3000,
        panelClass: ['error-snackbar']
      });
    });
  }
}
