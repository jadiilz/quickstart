import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvitationRegisterComponent } from './invitation-register.component';

describe('InvitationRegisterComponent', () => {
  let component: InvitationRegisterComponent;
  let fixture: ComponentFixture<InvitationRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvitationRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvitationRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
