import { Component, OnInit, ElementRef } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators, FormBuilder, FormGroupDirective } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Services
 */
// import { ArrayService } from '../../../shared/services/array.service';
import { CrudService } from '../../../shared/services/laravel/crud.service';
import { ValidateRequired } from '../../../shared/validators/required.validator';
// import { StringService } from '../../../shared/services/string.service';
// import { WindowService } from './../../../shared/services/window.service';

// import { ConfirmComponent } from '../../../shared/components/confirm/confirm.component';

@Component({
  selector: 'app-invitation-register',
  templateUrl: './invitation-register.component.html',
  styleUrls: ['./invitation-register.component.css']
})
export class InvitationRegisterComponent implements OnInit {
  public invitationForm: FormGroup;
  public paramToSearch: any;
  public profileArray: any;
  public submitButton: string;
  public submitToCreate: boolean;
  public submitToUpdate: boolean;
  public title: string;
  public widthHalf: string;
  public widthThird: string;
  public widthQuarter: string;
  loading: boolean;


  constructor(
    // private _array: ArrayService,
    private _crud: CrudService,
    public dialog: MatDialog,
    private _el: ElementRef,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    public _snackbar: MatSnackBar,
    // private _string: StringService,
    // private _windowService: WindowService
  ) {

  }

  ngOnInit() {

    this.invitationForm = this._formBuilder.group({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'access_profiles': new FormControl([], [ValidateRequired]),
    });

    this.listProfiles();

    this.invitationFormInit();

  }

  getErrorMessage () {
    return this.invitationForm.get('email').hasError('required') ? 'Campo obrigatório' :
    this.invitationForm.get('email').hasError('email') ? 'Email com formato inválido' : '';
  }

  addProfileId = (event) => {

    if (event.checked) {
      this.invitationForm.value.access_profiles.push(event.source.value);
    } else {

      this.invitationForm.value.access_profiles.splice(this.invitationForm.value.access_profiles.findIndex((el, i) => {
        return el === event.source.value; }), 1);
    }
  }

  invitationFormInit = () => {
    this._route.params.subscribe(params => {
      if (params.id) {
        this.paramToSearch = params.id;
        this.submitToCreate = false;
        this.submitToUpdate = true;
        this.title = 'Atualizar convite';
        this.submitButton = 'Atualizar';

        const param = this.paramToSearch.replace(':', '');

        this._crud
        .newRead({
          route: 'invitation/' + param,
        }).then(resI => {
          // console.log(resI);
          this.invitationForm.patchValue(resI['obj'][0]);

          // this._crud
          // .read({
          //   route: 'permissive-inviters'
          // }).then(resP => {
          //   this.profileArray = resP['obj'];
          //   for (let lim = resI['obj'][0]['profiles'].length, i = 0; i < lim; i++) {
          //     for (let limP = this.profileArray.length, j = 0; j < limP; j++) {
          //       if (resI['obj'][0]['profiles'][i]['id'] === this.profileArray[j]['id']) {
          //         this.profileArray[j]['_checked'] = true;
          //         this.invitationForm.controls.profiles.value.push(this.profileArray[j]['id']);
          //       }
          //     }
          //   }
          // });
        });
      } else {
        this.submitToCreate = true;
        this.submitToUpdate = false;
        this.title = 'Cadastrar convite';
        this.submitButton = 'Cadastrar';
      }
    });
  }

  onInvitationSubmit = () => {
    if (this.submitToUpdate) {
      const params = {
        route: 'invitation',
        objectToUpdate: this.invitationForm.value,
        paramToUpdate: this.paramToSearch.replace(':', '')
      };


      // const dialogRef = this.dialog.open(ConfirmComponent, {
      //   width: '350px',
      //   data: {
      //     dialogMessage: 'Deseja reenviar o convite?'
      //   }
      // });
      // dialogRef.afterClosed().subscribe(result => {
      //   if (result === true) {
      //     this._crud
      //     .update({
      //       route: 'register-update',
      //       objectToUpdate: {
      //         invited_status_id: 2,
      //       },
      //       paramToUpdate: params.paramToUpdate
      //     }).then(res => {
      //       let paramsB = {
      //         route: 'inviteds',
      //         objectToCreate: this.invitationForm.value
      //       };

      //       this._crud.create(paramsB)
      //       .then(res => {
      //         let message = this._string.replaceAll(res['message'], '"', '');

      //         this._snackbar.open(message, '', {
      //           duration: 5000,
      //           panelClass: ['success-snackbar']
      //         })
      //         this._router.navigate(['/main/invitation']);
      //       }, rej => {
      //         this._crud
      //         .updateWithPatch({
      //           route: 'register-update',
      //           objectToUpdate: {
      //             invited_status_id: 1,
      //           },
      //           paramToUpdate: params.paramToUpdate
      //         })
      //         let message = this._string.replaceAll(rej['message'], '"', '');
      //         this._snackbar.open(message, '', {
      //           duration: 5000,
      //           panelClass: ['error-snackbar']
      //         })
      //       })
      //     })
      //   }
      // });

    } else if (this.submitToCreate) {
      const params = {
        route: 'invitation',
        objectToCreate: this.invitationForm.value
      };
      this._crud.create(params)
      .then(res => {
        const message = res['apiBody']['message'];
        this._snackbar.open(message, '', {
          duration: 5000,
          panelClass: ['success-snackbar']
        });

        // this.invitationForm.reset();
        // for (var name in this.invitationForm.controls) {
        //   this.invitationForm.controls[name].setErrors(null);
        // }

        // Over dynamic checkboxes: start
        for (let lim = this.profileArray.length, i = 0; i < lim; i++) {
          this.profileArray[i]['_checked'] = false;
        }
        // Over dynamic checkboxes: end
      }, rej => {
        const message = rej['message'];

        this._snackbar.open(message, '', {
          duration: 5000,
          panelClass: ['error-snackbar']
        });
      });

    } else {
      console.log('No action defined on submit');
    }
  }

  listProfiles = () => {
    this._crud.newRead({route: 'profile'}).then(res => {
      this.loading = false;
      this.profileArray = res['obj'];
    });
  }

  clearForm = (formDirective: FormGroupDirective) => {
    formDirective.resetForm();
    const matHints = document.querySelectorAll('mat-hint');
    for (let hint = 0; hint < matHints.length; hint++) {
      // if(typeof(matHints[hint])=="object")
      matHints[hint].remove();
    }
    for (let lim = this.profileArray.length, i = 0; i < lim; i++) {
      this.profileArray[i]['_checked'] = false;
      this.profileArray[i]['_disabled'] = false;
    }
    this.invitationForm.value.profiles = [];
  }
}
