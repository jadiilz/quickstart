import { Component, OnInit, ElementRef } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, FormGroupDirective } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

/**
 * Services
 */
import { AuthenticationService } from '../../../shared/services/laravel/authentication.service';
import { CrudService } from '../../../shared/services/laravel/crud.service';
import { ValidateRequired } from '../../../shared/validators/required.validator';

@Component({
  selector: 'app-invitation',
  templateUrl: './invitation.component.html',
  styleUrls: ['./invitation.component.css']
})
export class InvitationComponent implements OnInit {
  public mask: any;
  public paramToSearch: any;
  public paramsToTableData: any;
  public title: string;
  private user: any;
  loading: boolean;
  public userPemissions: any;

  constructor(
    private auth: AuthenticationService,
    private _crud: CrudService,
    private _route: ActivatedRoute,
    public _snackbar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.loading = true;

     this.auth.setUser()
    .then(res => {
      this.loading = false;
      this.user = res;
      this.userPemissions =  this.auth.getPermissions('/main/invitation', this.user);
      this.makeList();
    });
  }


  makeList = () => {
    this.loading = false;
    this.paramsToTableData = {
      toolbar: {
        title: 'Convites',
        delete: {
          routeAfterDelete: '/main/invitation',
          routeToApi: 'invitations',
          justify: false,
          fieldToDelete: 'id',
          deleteMessage: 'ATENÇÃO: Deseja realmente excluir o(s) convite(s) selecionado(s) ?'
        },
        search: {
          advancedSearch: false
        },
        actionButton: {
          type: 'raised',
          value: 'CONVIDAR USUÁRIO',
          color: 'accent'
        },
      },
      list: {
        route: 'invitation',
        limit: 5,
        order: 'id,desc',
        columns: [
          { columnDef: 'created_at', header: 'Gerado em', cell: (row: Invitation) => `${row.created_at}` },
          { columnDef: 'email', header: 'Convidado', cell: (row: Invitation) => `${row.email == null ? '' : row.email}`},
          // { columnDef: 'inviter', header: 'Gerado por', cell: (row: Invitation) => `${row.inviter}`}
        ],
        edit: {route: '/main/invitation-register/', param: 'id'},
        permissions: this.userPemissions
      },
      actionToolbar: {
        language: 'pt-br'
      }
    };
  }

  clearForm = (formDirective: FormGroupDirective) => {
    formDirective.resetForm();
    const matHints = document.querySelectorAll('mat-hint');
    for (let hint = 0; hint < matHints.length; hint++) {
      matHints[hint].remove();
    }
  }

  eventUserInvitation = (event) => {
    if (event.referenceToAction === 'CONVIDAR USUÁRIO') {
      this.router.navigate(['/main/invitation-register']);
    }
  }


}

export interface Invitation {
  created_at: string;
  email: any;
  inviter: any;
}
