import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/laravel/authentication.service';

/**
 * Services
 */
// import {WindowService} from './../../../shared/services/window.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  public widthHalf: string;
  public widthThird: string;
  public widthQuarter: string;
  public userPemissions;
  public paramsToTableData: any;
  loading: boolean;
  user: any;

  constructor(
    private _router: Router,
    private auth: AuthenticationService
    // private _windowService: WindowService
  ) {}

  ngOnInit() {
    this.loading = true;

     this.auth.setUser()
    .then(res => {
      this.loading = false;
      this.user = res;
      this.userPemissions =  this.auth.getPermissions('/main/profile', this.user);
      this.makeList();
    });
  }

  eventProfileadd = (event) => {
    if (event.referenceToAction === 'ADICIONAR PERFIL') {
      this._router.navigate(['/main/profile-register']);
    }
  }

  makeList = () => {
    this.loading = false;
    this.paramsToTableData = {
      toolbar: {
        title: 'Perfis',
        delete: {
          routeAfterDelete: '/main/profile',
          routeToApi: 'profile',
          fieldToDelete: 'id',
          deleteMessage: 'ATENÇÃO: Deseja realmente excluir o(s) perfil(s) selecionado(s) ?'
        },
        search: {
          advancedSearch: false
        },
        actionButton: {
          type: 'raised',
          value: 'ADICIONAR PERFIL',
          color: 'accent'
        },
      },
      list: {
        route: 'profile',
        limit: 5,
        columns: [
          { columnDef: 'description', header: 'Perfil', cell: (row: Profile) => `${row.description}` }
        ],
        edit: {route: '/main/profile-update/', param: 'id'},
        permissions: this.userPemissions
      }
    };
  }
}

export interface Profile {
  description: string;
}
