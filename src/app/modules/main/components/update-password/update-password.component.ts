import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MatSnackBar } from '@angular/material';

/**
 * Services
 */
import { CrudService } from './../../../shared/services/laravel/crud.service';
import { AuthenticationService } from '../../../shared/services/laravel/authentication.service';
import { ValidateValueComparison } from '../../../shared/validators/value-comparison.validator';
import { ValidateRequired } from '../../../shared/validators/required.validator';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.component.html',
  styleUrls: ['./update-password.component.css']
})
export class UpdatePasswordComponent implements OnInit {
  passwordErrors: any;
  checkCurrentPassword: any;
  updatePassForm: FormGroup;
  user: any;

  constructor(
    private _auth: AuthenticationService,
    private _crud: CrudService,
    private _snackbar: MatSnackBar,
    public dialogRef: MatDialogRef<UpdatePasswordComponent>
  ) { }

  ngOnInit() {
    this._auth
    .setUser()
    .then(res => {
      this.user = res;
    });

    this.updatePassForm = new FormGroup({
      'password': new FormControl(null, ValidateRequired),
      'newPassword': new FormControl(null),
      'repeatPassword': new FormControl(null, ValidateRequired),
    });

    this.updatePassForm.get('newPassword').setValidators([ValidateValueComparison(this.updatePassForm.get('repeatPassword'),
      'Campos nova senha e confirme nova senha precisam ter o mesmo valor'), ValidateRequired]);
  }

  onUpdatePassSubmit = () => {
    this._crud
    .create({
      route: 'check-passwords',
      objectToCreate: {
        id: this.user.id,
        password: this.updatePassForm.get('password').value
      }
    }).then(res => {
      this._crud.update({
        route: 'user',
        objectToUpdate: {
          password: this.updatePassForm.get('newPassword').value
        },
        paramToUpdate: this.user.id
      }).then( resp => {
        this._snackbar.open('SUCESSO: Salvo com sucesso', '', {
          duration: 4000,
          panelClass: ['success-snackbar']
        });
        this.dialogRef.close();
      });

    }, rej => {
      this._snackbar.open(rej['errors'], '', {
        duration: 4000,
        panelClass: ['error-snackbar']
      });
    });
  }

  onPasswordChange = () => {
    const newpass = this.updatePassForm.value.password;
    const repeatpass = this.updatePassForm.value.newPassword;
    if ((newpass === repeatpass) && (newpass !== null && repeatpass !== null) ) {
      this.updatePassForm.get('password').setErrors(Validators.required);
      this.updatePassForm.get('newPassword').setErrors(Validators.required);
      this.passwordErrors = 'ERRO: O campo “Senha Atual” e o campo “Nova Senha” devem ser diferentes.';
    } else {
      if ((newpass && repeatpass) && (newpass.value !== '' && repeatpass.value !== '')) {
        this.updatePassForm.get('password').setErrors(null);
        this.updatePassForm.get('newPassword').setErrors(null);
        this.passwordErrors = null;
      }
    }
  }
  close = () => {
    this.dialogRef.close();
  }
}
