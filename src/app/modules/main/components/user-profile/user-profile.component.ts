import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

/**
 * Services
 */
import { AuthenticationService } from '../../../shared/services/laravel/authentication.service';
import { CrudService } from './../../../shared/services/laravel/crud.service';

import { UpdatePasswordComponent } from '../update-password/update-password.component';
import { SnackBarService } from '../../../shared/services/snackbar.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  colSize: number;
  loading: boolean;
  mask: any = {
    cpf: [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/],
    date: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
    zip: [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/],
    phone: ['(', /\d/, /\d/, ')', ' ' , /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, ],
    cell_phone: ['(', /\d/, /\d/, ')', ' ' , /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    cnpj: [/\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/]
  };
  user: any;
  userProfileForm: FormGroup;
  passwordErrors: any;
  public widthHalf: string;
  public widthThird: string;
  public widthQuarter: string;
  public phoneErrors: any;
  public ufArray: any;

  constructor(
    private authentication: AuthenticationService,
    private _crud: CrudService,
    public _snackbar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private _dialog: MatDialog,
    public snackBarService: SnackBarService
  ) {
  }

  /**
   * Cycle hook actions start
   */
  ngOnInit() {
    this.loading = true;

    (window.innerWidth < 450) ? this.colSize = 2 : this.colSize = 1;

    this.authentication
    .setUser()
    .then(res => {
      this.loading = false;
      this.user = res;
      console.log(this.user);
      delete this.user.password;

      this.userProfileForm.patchValue(this.user);

      // this._crud.read({
      //   route: 'user/' + this.user.id,
      //   // where: [{
      //   //   field: 'id',
      //   //   value: this.user.id
      //   // }]
      // })
      // .then(resp => {
      //    console.log(resp);
      // });
    });

    this.userProfileForm = new FormGroup({
      'name': new FormControl(null, [Validators.required, Validators.maxLength(191)]),
      'email': new FormControl(null, [Validators.required, Validators.maxLength(191), Validators.email]),
    });

  }


  onUserProfileFormSubmit = () => {
    const objUpdate = this.userProfileForm.value;
    objUpdate.id = this.user.id;

    this._crud.update({
      route: 'user',
      objectToUpdate: objUpdate,
      paramToUpdate: this.user.id
    }).then(res => {
      const message = 'foi';
      this._snackbar.open(message, '', {
        duration: 4000,
        panelClass: ['success-snackbar']
      });
    }, rej => {
      for (let i = 0; i < rej['errors'].length; i++) {
        this.snackBarService.add(rej['errors'][i]);
      }
    });
  }

  onUpdatePass = () => {
    const dialogRef = this._dialog.open(UpdatePasswordComponent, {
      width: '900px'
    });

  }

}
