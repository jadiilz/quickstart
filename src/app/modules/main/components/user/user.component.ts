import { Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import {MatDialogRef, MAT_DIALOG_DATA, MatSnackBar} from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

/**
 * Services
 */
import {CrudService} from './../../../shared/services/laravel/crud.service';
import { AuthenticationService } from '../../../shared/services/laravel/authentication.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  public userForm: FormGroup;
  title: 'Usuários';
  passwordErrors: any;
  loading: boolean;
  user: any;

  public paramToSearch: any;
  public submitToCreate: boolean;
  public submitToUpdate: boolean;
  public submitButton: string;
  public paramsToTableData: any;
  public userPemissions: any;

  constructor(
    private _personFormBuilder: FormBuilder,
    private crud: CrudService,
    public matsnackbar: MatSnackBar,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.loading = true;

     this.auth.setUser()
    .then(res => {
      this.loading = false;
      this.user = res;
      this.userPemissions =  this.auth.getPermissions('/main/user', this.user);
      this.makeList();
    });

  }

  makeList = () => {
    this.loading = false;
    this.paramsToTableData = {
      toolbar: {
        title: 'Usuários',
        // actionButton: [{
        //   permissions: ['CREATE', 'UPDATE'],
        //   type: 'raised',
        //   value: 'ADICIONAR USUÁRIO',
        //   color: 'accent'
        // }],
        delete: {
          routeAfterDelete: '/main/user',
          routeToApi: 'users',
          fieldToDelete: 'id',
          deleteMessage: 'ATENÇÃO: Deseja realmente desativar o(s) usuário(s) selecionado(s) ?',
          // exception: [{
          //   field: 'name',
          //   value: 'João'
          // }, {
          //   field: 'email',
          //   value: 'forcada@rule.com'
          // },
          // ]
        },
        search: {
          route: 'users',
          advancedSearch: true,
          format: [{
            field: 'name',
            placeholder: 'Nome',
            component: 'input'
          }]
        },
        actionButton: {
          type: 'raised',
          value: 'ADICIONAR USUÁRIO',
          color: 'accent'
        },
      },
      list: {
        route: 'users',
        limit: 10,
        columns: [
          { columnDef: 'name', header: 'Nome', cell: (row: User) => `${row.name}` },
          { columnDef: 'uf', header: 'UF', cell: (row: User) => `${row.uf == null ? '' : row.uf}`},
          { columnDef: 'email', header: 'Descrição', cell: (row: User) => `${row.email}` },
          { columnDef: 'profiles', header: 'Perfis de acesso', cell: (row: User) => `${row.access_profiles}` },
          { columnDef: 'deleted_at', header: 'Status', cell: (row: User) => `${row.is_active}`}
        ],
        edit: {
          route: '/main/user-update/',
          param: 'id',
          // exception: [{
          //   field: 'name',
          //   value: 'João'
          // }, {
          //   field: 'email',
          //   value: 'forcada@rule.com'
          // },
          // ]
        },
        permissions: this.userPemissions
      },
      actionToolbar: {
        language: 'pt-br'
      }
    };
  }

  eventUserParticipation = (event) => {
    if (event.referenceToAction === 'ADICIONAR USUÁRIO') {
      this.router.navigate(['/main/user-register']);
    }
  }
}

export interface User {
  name: string;
  uf: any;
  email: string;
  is_active: string;
  access_profiles: any;
}
