import {
  Component,
  OnInit,
  ChangeDetectorRef
} from '@angular/core';
import {
  MediaMatcher, BreakpointObserver, Breakpoints
} from '@angular/cdk/layout';

/**
 * Services
 */
import { AuthenticationService } from './../shared/services/laravel/authentication.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  private _mobileQueryListener: () => void;
  public options: any;
  public mobileQuery: MediaQueryList;
  user: any;
  items = [];
  menus = [];
  public mobile = (typeof window !== 'undefined') ?
    (window.screen.availWidth < 800) :
    true;
  public views: Object[] = [{
    name: 'Painel inicial',
    link: ['dashboard']
  }, {
    name: 'Usuários',
    link: ['user']
  },
  {
    name: 'Menu',
    link: ['menu']
  }
  ];

  constructor(
    private _auth: AuthenticationService,
    public breakpointObserver: BreakpointObserver,
    public changeDetectorRef: ChangeDetectorRef,
    public media: MediaMatcher
  ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();

    this
      .mobileQuery
      .addListener(this._mobileQueryListener);

    this.options = {
      fixed: 'fixed', // Define the sidenav style, possible values are 'fixed' and 'Non-fixed'
      opened: !this.mobileQuery.matches, // Possible values are true or false
      mode: 'auto', // Define the sidenav mode, possible values are push, side and over
      top: 56, // Css 'top' from sidenav
      bottom: 0, // Css 'bottom' from sidenav
      userCard: true
    };

    breakpointObserver.observe([
      Breakpoints.HandsetLandscape,
      Breakpoints.HandsetPortrait
    ]).subscribe(mobile => {
      this.options.opened = !mobile.matches;
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy(): void {
    this
      .mobileQuery
      .removeListener(this._mobileQueryListener);
  }

  ngOnInit() {

    this._auth
    .setUser()
    .then(res => {
      this.user = res;
      this.setUserMenu();
    });
   }

  logout = () => {
    let params;
    params = {
      navigateTo: '/'
    };
  }

  setUserMenu() {
    this.user.access_profiles.map( a => {
      a.menu_rules.map( mr => {
        if (mr.menu[0] && this.items.filter( i => i.title === mr.menu[0].description).length === 0 && mr.rule[0].name === 'Menu') {
          if (!this.hasAForcedRemoveMenu(mr)) {
            const icon = 'nb-gear';
            const itemMenu = {
              title: mr.menu[0].description,
              icon: icon,
              link: [mr.menu[0].route],
            };
            this.items.push(itemMenu);
          }
        }
      });
    });

    this.user.menu_rules.map(mr => {
      if (mr.menu[0] && this.items.filter( i => i.title === mr.menu[0].description).length === 0 && mr.rule[0].name === 'Menu' &&
      mr.pivot.modification === 'ADDED') {
        const icon = 'nb-gear';
          const itemMenu = {
            title: mr.menu[0].description,
            icon: icon,
            link: [mr.menu[0].route],
          };
          this.items.push(itemMenu);
      }
    });

  }

  hasAForcedRemoveMenu(rule) {
    return this.user.menu_rules.filter(mr => mr.menu[0].route === rule.menu[0].route
      && mr.pivot.modification === 'REMOVED' && mr.name.includes('menu')).length === 1;
  }
}
