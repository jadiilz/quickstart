import { Component, OnInit, ViewChild, Input, ViewChildren, QueryList, Output, EventEmitter } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource, MatPaginatorIntl, MatDialog,
  MatColumnDef, MatInput, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { DeleteConfirmComponent } from '../delete-confirm/delete-confirm.component';
import { CrudService } from '../../services/laravel/crud.service';
import { FormGroup, FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-ntm-table-data',
  templateUrl: './ntm-table-data.component.html',
  styleUrls: ['./ntm-table-data.component.css'],
  providers: [MatPaginatorIntl]
})
export class NtmTableDataComponent implements OnInit {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('searchKey', { read: MatInput }) searchKeyFocus: MatInput;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChildren('myCheckbox') private myCheckboxes: QueryList<any>;
  @Input() params;
  @Input('matColumnDef') public matColumnDef: MatColumnDef;
  @Output() actionToOutput = new EventEmitter();
  public progress = '';
  color = 'primary';
  mode = 'indeterminate';
  diameter = 5;
  displayedColumns = [];
  dataSource = new MatTableDataSource([]);
  searchInput = false;
  limit = 5;
  prepositionLabel = 'de';
  checkedItem = false;
  arraySource: any = [];
  itensToDeleteIds: any = [];
  itensToCredential: any = [];
  checkAllController = false;
  public checkAllItens;
  public iconDelete = 'lock';
  public justify = false;
  public includeDelete = 'Remover';
  public notEdit = false;
  public isLoadingList = true;
  public searching = false;
  public pesquisa: any = {};
  public filteredOptions: any = {};
  public filtereds: any = {};
  public pesquisaArray: any = [];
  cont = 0;
  public advancedSearchForm: FormGroup;

  constructor(
    private _crud: CrudService,
    private _paginator: MatPaginatorIntl,
    public dialog: MatDialog,
    public _snackbar: MatSnackBar,
    private router: Router
  ) {
    this._paginator.nextPageLabel = 'Próximo';
    this._paginator.previousPageLabel = 'Anterior';
    this._paginator.itemsPerPageLabel = 'Itens por página';
    this._paginator.getRangeLabel = (page: number, pageSize: number, length: number) => {
      if (length === 0 || pageSize === 0) { return `0 ${this.prepositionLabel} ${length}`; }
      length = Math.max(length, 0);
      const startIndex = page * pageSize;
      // If the start index exceeds the list length, do not try and fix the end index to the end.
      const endIndex = startIndex < length ?
          Math.min(startIndex + pageSize, length) :
          startIndex + pageSize;

      return `${startIndex + 1} - ${endIndex} ${this.prepositionLabel} ${length}`;
    };
   }

  columns = [];

  ngOnInit() {

    const object = {};
    if (this.params.toolbar.search.format) {
      this.params.toolbar.search.format.forEach(element => {
        object[element.field] = new FormControl(null);
      });
      this.advancedSearchForm = new FormGroup(object);

    }

    if (!this.params.list.permissionsOuther) {
      this.params.list.permissionsOuther = [];
    }
    if (this.params.list.limit) {
      this.limit = this.params.list.limit;
    }
    this.columns = this.params.list.columns;
    if (this.params.toolbar.delete.icon) {
      this.iconDelete = this.params.toolbar.delete.icon;
      this.includeDelete = 'Editar';
    }
    if (this.params.list.notEdit) {
      this.notEdit = true;
    }
    if (this.params.list.permissions.includes(this.includeDelete)) {
      this.displayedColumns.push('id');
    }
    this.columns.map(x => this.displayedColumns.push(x.columnDef));
    if (this.params.list.permissions.includes('Editar') && !this.notEdit) {
      this.displayedColumns.push('updated_at');
    }
    if (!this.params.list.edit) {
      this.displayedColumns.splice(0, 1);
    }
    this.readData();
  }

  // ngOnChanges(): void {
  //   this.readData();
  //   if(!this.params.list.permissionsOuther)
  //     this.params.list.permissionsOuther = [];
  // }

  readData() {
    this._crud.newRead({route: this.params.list.route}).then(res => {
      this.arraySource = res['obj'];
      for (let i = 0, lim = this.arraySource.length; i < lim; i++ ) {
        if (typeof(this.arraySource[i].is_active) !== 'undefined') {
          this.arraySource[i].is_active = this.arraySource[i].is_active ? 'Ativo' : 'Inativo';
        }
        if (typeof(this.arraySource[i].access_profiles) !== 'undefined' && this.arraySource[i].access_profiles != null) {
          this.arraySource[i].access_profiles = this.arraySource[i].access_profiles.map(p => p.description);
        }
      }
      this.dataSource = new MatTableDataSource(this.arraySource);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.isLoadingList = false;
    }, rej => {
      this.isLoadingList = false;
    });
  }

  dateFormatToDMY = (date) => {
    if (date && date !== '') {
      const newDate = date.split('-'),
      day = newDate[2],
      month = newDate[1],
      year = newDate[0];

      const final = day + '/' + month + '/' + year;
      return final;
    }
    return '';
  }

  dateFormatToDMYHM = (date) => {
    if (date && date !== '') {
        let dateFinal = '';
        try {
          const newDate = new Date(date);
          dateFinal = newDate.toLocaleString();
        } catch (error) {
          console.log(error);
        }

      return dateFinal;
    }
    return '';
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    sessionStorage.setItem('pesquisa', filterValue);
    this.dataSource.filter = filterValue;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }

  searchInputToggle = () => {
    this.searchInput = !this.searchInput;
    this.searchKeyFocus.focus();
    if (!this.searchInput) {
      this.searchKeyFocus.value = '';
      this.readData();
    }
  }

  checkItem = (id, e) => {
    const count = 0;
    if (e.checked) {
      this.checkAllController = e.checked;
      this.itensToDeleteIds.push(id);
      const myCheckboxes = this.myCheckboxes.toArray();
      let all = true;
      for (let i = 0; i < myCheckboxes.length; i++) {
       if (!myCheckboxes[i]._checked) { all = false; }
      }

      if (all) { this.checkAllItens = true; }
    } else {
      this.itensToDeleteIds.splice(this.itensToDeleteIds.indexOf(id), 1);
      this.checkAllItens = e.checked;
      if (this.itensToDeleteIds.length === 0) {
        this.checkAllController = e.checked;
      }
    }
  }

  checkAllToggle = (event) => {
    this.checkAllController = event.checked;
    this.checkAllItens = event.checked;

    if (event.checked) {
      this.checkAll();
    } else {
      this.uncheckAll();
    }
  }

  checkAll = () => {
    const myCheckboxes = this.myCheckboxes.toArray();
    for (let i = 0; i < myCheckboxes.length; i++) {
      this.itensToDeleteIds.push(myCheckboxes[i].value);
      myCheckboxes[i]._checked = true;
    }
  }

  uncheckAll = () => {
    const myCheckboxes = this.myCheckboxes.toArray();
    for (let i = 0; i < myCheckboxes.length; i++) {
      this.itensToDeleteIds.splice(this.itensToDeleteIds.indexOf(myCheckboxes[i].value), 1);
      myCheckboxes[i]._checked = false;
    }
  }

  onClickEdit = (route, param) => {
    const finalRoute = [route + ':' + param.id];
    // let finalRoute = [route + param.id]
    this.router.navigate(finalRoute);
  }

  openDialogToDelete = (fieldToUseInDelete, element) => {
    let dialogMessage = '', height = '250px';
    if (this.params.toolbar && this.params.toolbar.delete.deleteMessage) {
      dialogMessage = this.params.toolbar.delete.deleteMessage;
    }

    if (this.params.toolbar.delete.justify) {
      this.justify = this.params.toolbar.delete.justify;
      height = '303px';
    }

    const arrItems = [];
    if (element) {
      this.itensToDeleteIds.push(element.id);
    }

    const dialogRef = this.dialog.open(DeleteConfirmComponent, {
      width: '350px',
      height: height,
      data: {
        routeToApi: this.params.toolbar.delete.routeToApi,
        routeAfterDelete: this.params.toolbar.delete.routeAfterDelete,
        paramToDelete: this.itensToDeleteIds,
        justify: this.justify,
        dialogMessage: dialogMessage
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const array = '';
      const string = '';
      this.uncheckAll();
      this.readData();
      this.setOutput('toggle');
      this.checkAllController = false;
    });

  }

  setOutput = (value) => {
    const response = {
      referenceToAction: value
    };
    this.actionToOutput.emit(response);
  }

  checkException = (element, type) => {
    let exceptions, retorno;
    const aux = [];
    if (type === 'Delete') {
      this.params.toolbar.delete.exception ? exceptions = this.params.toolbar.delete.exception : exceptions = null;
    } else {
      this.params.list.edit.exception ? exceptions = this.params.list.edit.exception : exceptions = null;
    }
    if (exceptions !== null) {
      for (let x = 0; x < exceptions.length; x++) {
        if (exceptions[x].value === element[exceptions[x].field] ) {
            aux.push(element);
        }
      }
      (aux.length > 0 ) ? retorno =  false : retorno = true;
      return retorno;
    } else {
      return true;
    }
  }

  openSearch(event) {
    this.make();
  }

  middleware() {
    if (this.cont < this.params.toolbar.search.format.length - 1) {
      this.cont++;
      this.make();
    } else {
      this.isLoadingList = false;
      this.searching = true;
    }
  }

  filter(name, param, filter) {
    return name ? param.filter(s => typeof s[filter] !== 'object' ? s[filter].toLowerCase().indexOf(name.toLowerCase()) === 0 :
    s[filter]['description'].toLowerCase().indexOf(name.toLowerCase()) === 0) : param;
  }
  make() {
    let params = '', post = false;
    const element = this.params.toolbar.search.format[this.cont];
    if (element.search) { params = element.search; post = true; }
    if (element.route) {
      this.pesquisa[element.field] = new FormControl();
      this._crud.read({ route: element.route, data: {search: params}, getByPost: post }).then(res => {
        this.pesquisaArray[element.field] = res['obj'];
        this.filtereds[element.field] = res['obj'];
        this.filteredOptions[element.field] = this.pesquisa[element.field].valueChanges
          .pipe(
            startWith('')
            // ,map(value => typeof value[element.filter] != 'object' ? value[element.filter] : value[element.filter]['description'] )
            , map(value => this.filter(value, this.filtereds[element.field], element.filter))
          );
          this.middleware();
      });
    } else {
      this.middleware();
    }
  }
}
